'use strict'
/* eslint-env mocha */

// Imports
const chai = require('chai')
const dirtyChai = require('dirty-chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const chaiAsPromised = require('chai-as-promised')

const fs = require('fs')
const yo = require('../../src/yeoman')
const composer = require('../../src/composer')
const dockerCompose = require('../../src/docker-compose')
const wp = require('../../src/wp-cli')
const ssh = require('../../src/ssh')
const Writable = require('stream').Writable
const path = require('path')

const Project = require('../../src/project/Project')
const Actions = require('../../src/actions')

const expect = chai.expect
chai.use(dirtyChai)
chai.use(sinonChai)
chai.use(chaiAsPromised)

describe('Project', function () {
  const environment = {
    workDir: 'myworkdir',
    hostWorkDir: 'hostworkdir'
  }

  let eventStream

  beforeEach(function () {
    sinon.stub(fs, 'mkdir').yieldsAsync(null)
    sinon.stub(yo, 'generate').resolves()
    sinon.stub(composer, 'install').resolves()
    sinon.stub(dockerCompose, 'up').resolves()
    sinon.stub(dockerCompose, 'run').resolves()

    eventStream = Writable({ objectMode: true })
    eventStream._write = sinon.stub().yields()
    sinon.spy(eventStream, 'write')
    sinon.spy(eventStream, 'end')
  })

  afterEach(function () {
    fs.mkdir.restore()
    yo.generate.restore()
    composer.install.restore()
    dockerCompose.up.restore()
    dockerCompose.run.restore()
  })

  describe('#createFromScratch', function () {
    const slug = 'my-slug'
    const projectDir = environment.workDir + '/' + slug
    const hostProjectDir = path.join(environment.hostWorkDir, slug)

    beforeEach(function () {
      sinon.stub(wp, 'coreInstall').resolves()
    })

    afterEach(function () {
      wp.coreInstall.restore()
    })

    describe('happy path', function () {
      beforeEach(function () {
        return Project.createFromScratch(
          environment,
          slug,
          'myadmin',
          'mypassword',
          'myemail@example.com',
          'mydeployhost',
          'mydeployuser',
          eventStream)
      })

      it('writes start event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'createFromScratch',
          params: {
            directory: hostProjectDir
          }
        })
      })

      it('creates a project directory under workDir', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'createProjectScaffolding',
          params: {directory: hostProjectDir}
        })
        expect(fs.mkdir.getCall(0).args[0]).to.equal(projectDir)
      })

      it('calls Yeoman to run the generator in the directory', function () {
        expect(yo.generate.getCall(0).args[0]).to.equal('generator-wysiwygoy-wordpress')
        expect(yo.generate.getCall(0).args[1]).to.equal(projectDir)
        expect(yo.generate.getCall(0).args[2]).to.equal('wysiwyg')
        expect(yo.generate.getCall(0).args[3]).to.deep.equal({
          databasePrefix: undefined,
          deployHost: 'mydeployhost',
          deployUser: 'mydeployuser'
        })
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.FINISH_TASK,
          source: 'createProjectScaffolding'
        })
      })

      it('runs composer install in the host directory', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.START_TASK, source: 'composerInstall'})
        expect(composer.install.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.FINISH_TASK, source: 'composerInstall'})
      })

      it('runs docker-compose up in the host directory', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.START_TASK, source: 'startServers'})
        expect(dockerCompose.up.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'startServers'})
      })

      it('waits for database to start', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.START_TASK, source: 'waitForDatabase'})
        expect(dockerCompose.run.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(dockerCompose.run.getCall(0).args[1]).to.equal('php-cli')
        expect(dockerCompose.run.getCall(0).args[2]).to.deep.equal([
          'wait-for-it.sh',
          `--host=${slug}-db`,
          '--port=3306',
          '--timeout=60'
        ])

        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'waitForDatabase'})
      })

      it('initializes WordPress using wp-cli with Docker', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({
          type: Actions.START_TASK,
          source: 'installWordPress'
        })
        expect(wp.coreInstall.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(wp.coreInstall.getCall(0).args[1]).to.equal(`${slug}.local.wysiwyg.fi`)
        expect(wp.coreInstall.getCall(0).args[2]).to.equal(slug)
        expect(wp.coreInstall.getCall(0).args[3]).to.equal('myadmin')
        expect(wp.coreInstall.getCall(0).args[4]).to.equal('mypassword')
        expect(wp.coreInstall.getCall(0).args[5]).to.equal('myemail@example.com')
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'installWordPress'})
      })

      it('writes finish event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.FINISH_TASK, source: 'createFromScratch'
        })
      })
    })

    describe('#createProjectScaffolding failure', function () {
      describe('fs#mkdir failure', function () {
        const error = new Error('fs#mkdir')
        let caught

        beforeEach(function () {
          fs.mkdir.yieldsAsync(error)

          return Project.createFromScratch(
            environment,
            slug,
            'myadmin',
            'mypassword',
            'myemail@example.com',
            'mydeployhost',
            'mydeployuser',
            eventStream
          ).catch(e => { caught = e })
        })

        it('rejects the top-level promise', function () {
          expect(caught.cause).to.equal(error)
        })

        it('sends failure event', function () {
          expect(eventStream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'createProjectScaffolding',
            error: caught
          })
        })
      })

      describe('yeoman#generate failure', function () {
        const error = new Error('yeoman#generate')
        let caught

        beforeEach(function () {
          yo.generate.rejects(error)

          return Project.createFromScratch(
            environment,
            slug,
            'myadmin',
            'mypassword',
            'myemail@example.com',
            'mydeployhost',
            'mydeployuser',
            eventStream
          ).catch(e => { caught = e })
        })

        it('rejects the top-level promise', function () {
          expect(caught).to.equal(error)
        })

        it('sends failure event', function () {
          expect(eventStream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'createProjectScaffolding',
            error
          })
        })
      })
    })

    describe('#composerInstall failure', function () {
      const error = new Error('#composerInstall')
      let caught

      beforeEach(function () {
        composer.install.rejects(error)

        return Project.createFromScratch(
          environment,
          slug,
          'myadmin',
          'mypassword',
          'myemail@example.com',
          'mydeployhost',
          'mydeployuser',
          eventStream
        ).catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'composerInstall',
          error
        })
      })
    })

    describe('#startServers failure', function () {
      const error = new Error('#startServers')
      let caught

      beforeEach(function () {
        dockerCompose.up.rejects(error)

        return Project.createFromScratch(
          environment,
          slug,
          'myadmin',
          'mypassword',
          'myemail@example.com',
          'mydeployhost',
          'mydeployuser',
          eventStream
        ).catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'startServers',
          error
        })
      })
    })

    describe('#waitForDatabase failure', function () {
      const error = new Error('#waitForDatabase')
      let caught

      beforeEach(function () {
        dockerCompose.run.rejects(error)

        return Project.createFromScratch(
          environment,
          slug,
          'myadmin',
          'mypassword',
          'myemail@example.com',
          'mydeployhost',
          'mydeployuser',
          eventStream
        ).catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'waitForDatabase',
          error
        })
      })
    })

    describe('#installWordPress failure', function () {
      const error = new Error('#installWordPress')
      let caught

      beforeEach(function () {
        wp.coreInstall.rejects(error)

        return Project.createFromScratch(
          environment,
          slug,
          'myadmin',
          'mypassword',
          'myemail@example.com',
          'mydeployhost',
          'mydeployuser',
          eventStream
        ).catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'installWordPress',
          error
        })
      })
    })
  })

  describe('#createFromExistingSite', function () {
    const slug = 'my-slug'
    const sshOptions = {
      host: 'my.host.example',
      username: 'myremoteuser',
      password: 'mypassword',
      privateKey: Buffer.from('myprivatekey')
    }
    const wordPressRoot = 'public_html'
    const projectDir = environment.workDir + '/' + slug
    const hostProjectDir = path.join(environment.hostWorkDir, slug)
    const containerProjectDir = path.join(environment.workDir, slug)
    const dbExportStream = { }
    const remoteSiteUrl = 'https://my.host.example'
    const localSiteUrl = `http://${slug}.local.wysiwyg.fi`

    beforeEach(function () {
      sinon.stub(fs, 'readFile').withArgs('myworkdir/my-slug/config/import-files').yieldsAsync(null, 'my-filters')
      sinon.stub(wp, 'configGet').resolves([{ key: 'table_prefix', value: 'myprefix' }])
      sinon.stub(wp, 'optionGet').resolves(remoteSiteUrl)
      sinon.stub(ssh, 'importFiles').resolves()
      sinon.stub(wp, 'dbExport').resolves(dbExportStream)
      sinon.stub(wp, 'dbImport').resolves()
      sinon.stub(wp, 'searchReplace').resolves()
    })

    afterEach(function () {
      fs.readFile.restore()
      wp.configGet.restore()
      wp.optionGet.restore()
      ssh.importFiles.restore()
      wp.dbExport.restore()
      wp.dbImport.restore()
      wp.searchReplace.restore()
    })

    describe('happy path', function () {
      beforeEach(function () {
        return Project.createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
      })

      it('writes start event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'createFromExistingSite',
          params: {
            host: sshOptions.host,
            username: sshOptions.username,
            remoteDirectory: wordPressRoot,
            localDirectory: hostProjectDir
          }
        })
      })

      it('gets the WordPress configuration from remote host', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'getWpConfig',
          params: {
            host: sshOptions.host,
            username: sshOptions.username,
            directory: wordPressRoot
          }
        })
        expect(wp.configGet.getCall(0).args[0]).to.equal(wordPressRoot)
        expect(wp.configGet.getCall(0).args[1]).to.deep.equal(sshOptions)
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'getWpConfig'})
      })

      it('creates a project directory under workDir', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'createProjectScaffolding',
          params: {directory: hostProjectDir}
        })
        expect(fs.mkdir.getCall(0).args[0]).to.equal(projectDir)
      })

      it('calls Yeoman to run the generator in the directory', function () {
        expect(yo.generate.getCall(0).args[0]).to.equal('generator-wysiwygoy-wordpress')
        expect(yo.generate.getCall(0).args[1]).to.equal(projectDir)
        expect(yo.generate.getCall(0).args[2]).to.equal('traditional')
        expect(yo.generate.getCall(0).args[3]).to.deep.equal({
          databasePrefix: 'myprefix',
          deployHost: sshOptions.host,
          deployUser: sshOptions.user
        })
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.FINISH_TASK,
          source: 'createProjectScaffolding'
        })
      })

      it('runs composer install in the host directory', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.START_TASK, source: 'composerInstall'})
        expect(composer.install.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.FINISH_TASK, source: 'composerInstall'})
      })

      it('runs docker-compose up in the host directory', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.START_TASK, source: 'startServers'})
        expect(dockerCompose.up.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'startServers'})
      })

      it('copies remote files in the project directory', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'importFiles',
          params: {
            host: sshOptions.host,
            username: sshOptions.username,
            remoteDirectory: wordPressRoot,
            localDirectory: hostProjectDir + '/site/public' // TODO: don't hardcode
          }
        })
        expect(ssh.importFiles.getCall(0).args[0]).to.deep.equal(sshOptions)
        expect(ssh.importFiles.getCall(0).args[1]).to.equal(wordPressRoot)
        expect(ssh.importFiles.getCall(0).args[2]).to.equal(containerProjectDir + '/site/public')
        expect(ssh.importFiles.getCall(0).args[3]).to.equal('my-filters')
        expect(ssh.importFiles.getCall(0).args[4]).to.equal(eventStream)
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'importFiles'})
      })

      it('waits for database to start', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({type: Actions.START_TASK, source: 'waitForDatabase'})
        expect(dockerCompose.run.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(dockerCompose.run.getCall(0).args[1]).to.equal('php-cli')
        expect(dockerCompose.run.getCall(0).args[2]).to.deep.equal([
          'wait-for-it.sh',
          `--host=${slug}-db`,
          '--port=3306',
          '--timeout=60'
        ])

        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'waitForDatabase'})
      })

      it('imports the database', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'importDatabase',
          params: {
            host: sshOptions.host,
            username: sshOptions.username
          }
        })

        expect(wp.dbExport.getCall(0).args[0]).to.equal(wordPressRoot)
        expect(wp.dbExport.getCall(0).args[1]).to.deep.equal(sshOptions)
        expect(wp.dbImport.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(wp.dbImport.getCall(0).args[1]).to.equal(dbExportStream)

        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.FINISH_TASK,
          source: 'importDatabase'
        })
      })

      it('gets the site URL from remote host', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'getSiteUrl',
          params: {
            host: sshOptions.host,
            username: sshOptions.username,
            remoteDirectory: wordPressRoot
          }
        })
        expect(wp.optionGet.getCall(0).args[0]).to.equal(wordPressRoot)
        expect(wp.optionGet.getCall(0).args[1]).to.equal('siteurl')
        expect(wp.optionGet.getCall(0).args[2]).to.deep.equal(sshOptions)
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'getSiteUrl'})
      })

      it('replaces urls in contents', function () {
        expect(eventStream.write).to.have.been.calledWithMatch({
          type: Actions.START_TASK,
          source: 'replaceUrls',
          params: { remoteSiteUrl, localSiteUrl }
        })
        expect(wp.searchReplace.getCall(0).args[0]).to.equal(hostProjectDir)
        expect(wp.searchReplace.getCall(0).args[1]).to.equal(remoteSiteUrl)
        expect(wp.searchReplace.getCall(0).args[2]).to.equal(localSiteUrl)
        expect(eventStream.write).to.have.been.calledWith({type: Actions.FINISH_TASK, source: 'replaceUrls'})
      })

      it('writes finish event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.FINISH_TASK, source: 'createFromExistingSite'
        })
      })
    })

    describe('#getWpConfig failure', function () {
      const error = new Error('#getWpConfig')
      let caught

      beforeEach(function () {
        wp.configGet.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'getWpConfig',
          error
        })
      })
    })

    describe('#createProjectScaffolding failure', function () {
      describe('fs#mkdir failure', function () {
        const error = new Error('fs#mkdir')
        let caught

        beforeEach(function () {
          fs.mkdir.yieldsAsync(error)

          return Project
            .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
            .catch(e => { caught = e })
        })

        it('rejects the top-level promise', function () {
          expect(caught.cause).to.equal(error)
        })

        it('sends failure event', function () {
          expect(eventStream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'createProjectScaffolding',
            error: caught
          })
        })
      })

      describe('yeoman#generate failure', function () {
        const error = new Error('yeoman#generate')
        let caught

        beforeEach(function () {
          yo.generate.rejects(error)

          return Project
            .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
            .catch(e => { caught = e })
        })

        it('rejects the top-level promise', function () {
          expect(caught).to.equal(error)
        })

        it('sends failure event', function () {
          expect(eventStream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'createProjectScaffolding',
            error
          })
        })
      })
    })

    describe('#composerInstall failure', function () {
      const error = new Error('#composerInstall')
      let caught

      beforeEach(function () {
        composer.install.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'composerInstall',
          error
        })
      })
    })

    describe('#startServers failure', function () {
      const error = new Error('#startServers')
      let caught

      beforeEach(function () {
        dockerCompose.up.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'startServers',
          error
        })
      })
    })

    describe('#importFiles failure', function () {
      const error = new Error('#importFiles')
      let caught

      beforeEach(function () {
        ssh.importFiles.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'importFiles',
          error
        })
      })
    })

    describe('#waitForDatabase failure', function () {
      const error = new Error('#waitForDatabase')
      let caught

      beforeEach(function () {
        dockerCompose.run.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'waitForDatabase',
          error
        })
      })
    })

    describe('#importDatabase failure', function () {
      describe('wp#dbExport failure', function () {
        const error = new Error('wp#dbExport')
        let caught

        beforeEach(function () {
          wp.dbExport.rejects(error)

          return Project
            .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
            .catch(e => { caught = e })
        })

        it('rejects the top-level promise', function () {
          expect(caught).to.equal(error)
        })

        it('sends failure event', function () {
          expect(eventStream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'importDatabase',
            error
          })
        })
      })

      describe('wp#dbImport failure', function () {
        const error = new Error('wp#dbImport')
        let caught

        beforeEach(function () {
          wp.dbImport.rejects(error)

          return Project
            .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
            .catch(e => { caught = e })
        })

        it('rejects the top-level promise', function () {
          expect(caught).to.equal(error)
        })

        it('sends failure event', function () {
          expect(eventStream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'importDatabase',
            error
          })
        })
      })
    })

    describe('#getSiteUrl failure', function () {
      const error = new Error('#getSiteUrl')
      let caught

      beforeEach(function () {
        wp.optionGet.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'getSiteUrl',
          error
        })
      })
    })

    describe('#replaceUrls failure', function () {
      const error = new Error('#replaceUrls')
      let caught

      beforeEach(function () {
        wp.searchReplace.rejects(error)

        return Project
          .createFromExistingSite(environment, sshOptions, wordPressRoot, slug, eventStream)
          .catch(e => { caught = e })
      })

      it('rejects the top-level promise', function () {
        expect(caught).to.equal(error)
      })

      it('sends failure event', function () {
        expect(eventStream.write).to.have.been.calledWith({
          type: Actions.TASK_FAILED,
          source: 'replaceUrls',
          error
        })
      })
    })
  })
})
