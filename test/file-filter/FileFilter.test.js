'use strict'
/* eslint-env mocha */

// Imports for testing
const chai = require('chai')
const dirtyChai = require('dirty-chai')

// Imports to test
const FileFilter = require('../../src/file-filter/FileFilter')

const expect = chai.expect
chai.use(dirtyChai)

describe('FileFilter', function () {
  describe('#includes', function () {
    const filter = FileFilter.fromString(
      '# comment\n' +
      '\n' +
      '- /site/public/exclude.me\n' +
      '+ /foo/include.me'
    )

    it('does not include a file matching an exclude rule', function () {
      expect(filter.includes('/site/public/exclude.me')).to.be.false()
    })

    it('includes a file matching an include rule', function () {
      expect(filter.includes('/foo/include.me')).to.be.true()
    })

    it('includes a file not matching any rule', function () {
      expect(filter.includes('some.random.file')).to.be.true()
    })
  })

  describe('exclude everything at end', function () {
    const filter = FileFilter.fromString(
      '- /site/public/exclude.me\n' +
      '+ /foo/include.me\n' +
      '- *'
    )

    it('does not include a file matching an exclude rule', function () {
      expect(filter.includes('/site/public/exclude.me')).to.be.false()
    })

    it('includes a file matching an include rule', function () {
      expect(filter.includes('/foo/include.me')).to.be.true()
    })

    it('does not include other files', function () {
      expect(filter.includes('some.random.file')).to.be.false()
    })
  })
})
