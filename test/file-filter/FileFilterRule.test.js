'use strict'
/* eslint-env mocha */

// Imports for testing
const chai = require('chai')
const dirtyChai = require('dirty-chai')

// Imports to test
const FileFilterRule = require('../../src/file-filter/FileFilterRule')

const expect = chai.expect
chai.use(dirtyChai)

describe('FileFilterRule', function () {
  describe('- /absolute/path', function () {
    const rule = FileFilterRule.fromString('- /site/public/wp-config.php')

    it('excludes a matching file', function () {
      expect(rule.excludes('/site/public/wp-config.php')).to.be.true()
    })

    it('does not include a matching file', function () {
      expect(rule.includes('/site/public/wp-config.php')).to.be.false()
    })

    it('does not exclude a file not matching', function () {
      expect(rule.excludes('/some-random-file')).to.be.false()
    })

    it('does not include a file not matching', function () {
      expect(rule.includes('/some-random-file')).to.be.false()
    })
  })

  describe('exclude /absolute/path', function () {
    const rule = FileFilterRule.fromString('exclude /site/public/wp-config.php')

    it('excludes a matching file', function () {
      expect(rule.excludes('/site/public/wp-config.php')).to.be.true()
    })

    it('does not include a matching file', function () {
      expect(rule.includes('/site/public/wp-config.php')).to.be.false()
    })

    it('does not exclude a file not matching', function () {
      expect(rule.excludes('/some-random-file')).to.be.false()
    })

    it('does not include a file not matching', function () {
      expect(rule.includes('/some-random-file')).to.be.false()
    })
  })

  describe('+ /absolute/path', function () {
    const rule = FileFilterRule.fromString('+ /site/public/wp-config.php')

    it('includes a matching file', function () {
      expect(rule.includes('/site/public/wp-config.php')).to.be.true()
    })

    it('does not exclude a matching file', function () {
      expect(rule.excludes('/site/public/wp-config.php')).to.be.false()
    })

    it('does not include a file not matching', function () {
      expect(rule.includes('/some-random-file')).to.be.false()
    })

    it('does not exclude a file not matching', function () {
      expect(rule.excludes('/some-random-file')).to.be.false()
    })
  })

  describe('include /absolute path', function () {
    const rule = FileFilterRule.fromString('+ /site/public/wp-config.php')

    it('includes a matching file', function () {
      expect(rule.includes('/site/public/wp-config.php')).to.be.true()
    })

    it('does not exclude a matching file', function () {
      expect(rule.excludes('/site/public/wp-config.php')).to.be.false()
    })

    it('does not include a file not matching', function () {
      expect(rule.includes('/some-random-file')).to.be.false()
    })

    it('does not exclude a file not matching', function () {
      expect(rule.excludes('/some-random-file')).to.be.false()
    })
  })

  describe('comment', function () {
    const rule = FileFilterRule.fromString('# comment')

    it('comment lines should return null rule', function () {
      expect(rule).to.be.null()
    })
  })

  describe('empty line', function () {
    const rule = FileFilterRule.fromString('  \t')

    it('empty lines should return null rule', function () {
      expect(rule).to.be.null()
    })
  })

  describe('- just-a-file-name', function () {
    const rule = FileFilterRule.fromString('- wp-config.php')

    it('excludes a matching file', function () {
      expect(rule.excludes('/site/public/wp-config.php')).to.be.true()
    })

    it('does not include a matching file', function () {
      expect(rule.includes('/site/public/wp-config.php')).to.be.false()
    })

    it('does not exclude a file not matching', function () {
      expect(rule.excludes('/some-random-file')).to.be.false()
    })

    it('does not include a file not matching', function () {
      expect(rule.includes('/some-random-file')).to.be.false()
    })
  })
})
