'use strict'
/* eslint-env mocha */

// Imports for testing
const Writable = require('stream').Writable
const chai = require('chai')
const dirtyChai = require('dirty-chai')
const expect = chai.expect
const sinon = require('sinon')
const sh = require('sinon-helpers')
const proxyquire = require('proxyquire')
const sinonChai = require('sinon-chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(dirtyChai)
chai.use(sinonChai)
chai.use(chaiAsPromised)

// Imports to stub
const ssh2 = require('ssh2')
const fs = require('fs-extra')

const Actions = require('../../src/actions')

describe('ssh', function () {
  let ssh
  let sshClient
  let sftp

  beforeEach(function () {
    sftp = {
      readdir: sinon.stub().yieldsAsync(null, []),
      fastGet: sinon.stub().yieldsAsync(null),
      fastPut: sinon.stub().yieldsAsync(null)
    }

    sshClient = sh.getSpyConstructor(ssh2.Client)
      .withStubs('connect', 'sftp')
      .afterCreation(instance => {
        instance.connect.callsFake(() => {
          setTimeout(() => { instance.emit('ready') }, 0)
        })

        instance.sftp.yieldsAsync(null, sftp)
      })

    ssh = proxyquire(
      '../../src/ssh',
      { ssh2: { Client: sshClient } }
    )

    sinon.stub(fs, 'mkdir').resolves()
    sinon.stub(fs, 'utimes').resolves()
  })

  afterEach(function () {
    fs.mkdir.restore()
    fs.utimes.restore()
  })

  describe('#importFiles', function () {
    const connectionOptions = {
      host: 'myhost',
      username: 'myuser'
    }
    let stream

    function file (name, atime, mtime) {
      return {
        filename: name,
        attrs: {
          mode: fs.constants.S_IFREG,
          atime,
          mtime
        }
      }
    }

    function directory (name, atime, mtime) {
      return {
        filename: name,
        attrs: {
          mode: fs.constants.S_IFDIR,
          atime,
          mtime
        }
      }
    }

    beforeEach(function () {
      stream = Writable({objectMode: true})
      stream._write = sinon.stub().yields()
      sinon.spy(stream, 'write')
    })

    it('passes connection options to ssh2', function () {
      return ssh.importFiles(connectionOptions, 'source', 'destination', null, stream).then(() => {
        const { host, username } = connectionOptions
        expect(stream.write).to.have.been.calledWith({
          type: Actions.START_TASK,
          source: 'connect',
          params: { host, username }
        })
        expect(sshClient.getInstance().connect).to.have.been.calledWith(connectionOptions)
        expect(stream.write).to.have.been.calledWith({
          type: Actions.FINISH_TASK,
          source: 'connect'
        })
      })
    })

    it('handles connect failure', function () {
      const error = new Error()
      sshClient.afterCreation(instance => {
        instance.connect.callsFake(() => {
          setTimeout(() => { instance.emit('error', error) }, 0)
        })
      })

      return expect(ssh.importFiles(connectionOptions, 'source', 'destination', null, stream)).to.be.rejectedWith(error)
        .then(() => {
          expect(stream.write).to.have.been.calledWith({
            type: Actions.TASK_FAILED,
            source: 'connect',
            error
          })
        })
    })

    describe('with empty directory', function () {
      beforeEach(function () {
        sftp.readdir.withArgs('source').yieldsAsync(null, [])
      })

      it('creates the local directory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(stream.write).to.have.been.calledWith({
            type: Actions.START_TASK,
            source: 'importDirectory',
            params: { directory: 'source' }
          })

          expect(fs.mkdir.calledOnce).to.be.true()
          expect(fs.mkdir).to.have.been.calledWith('destination')

          expect(stream.write).to.have.been.calledWith({
            type: Actions.FINISH_TASK,
            source: 'importDirectory'
          })
        })
      })

      it('does not import any files', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(sftp.fastGet).to.not.have.been.called()
        })
      })
    })

    describe('with no subdirectories', function () {
      beforeEach(function () {
        sftp.readdir.withArgs('source').yieldsAsync(null, [ file('myfile', 12345678, 11223344) ])
      })

      it('creates the local directory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(fs.mkdir).to.have.been.calledOnce()
          expect(fs.mkdir).to.have.been.calledWith('destination')
        })
      })

      it('imports the file found in the directory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(stream.write).to.have.been.calledWith({
            type: Actions.START_TASK,
            source: 'importFile',
            params: { file: 'source/myfile' }
          })

          expect(sftp.fastGet).to.have.been.calledOnce()
          expect(sftp.fastGet).to.have.been.calledWith('source/myfile', 'destination/myfile')
          expect(sftp.fastGet).to.have.been.calledAfter(fs.mkdir)

          expect(stream.write).to.have.been.calledWith({
            type: Actions.FINISH_TASK,
            source: 'importFile'
          })
        })
      })

      it('preserves the timestamps of file', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(fs.utimes).to.have.been.calledWith('destination/myfile', 12345678, 11223344)
        })
      })

      it('does not import an excluded file', function () {
        const filters = '- /myfile'
        return ssh.importFiles({}, 'source', 'destination', filters, stream).then(() => {
          expect(sftp.fastGet).to.have.not.been.called()
          expect(stream.write).to.have.been.calledWith({
            type: Actions.SKIP_FILE,
            source: 'importFile',
            params: { file: 'source/myfile' }
          })
        })
      })
    })

    describe('with a subdirectory', function () {
      beforeEach(function () {
        sftp.readdir.withArgs('source').yieldsAsync(null, [ directory('mysubdir', 12312312, 22322312) ])
        sftp.readdir.withArgs('source/mysubdir').yieldsAsync(null, [ file('myfile') ])
      })

      it('creates the local subdirectory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(fs.mkdir).to.have.been.calledWith('destination/mysubdir')
        })
      })

      it('imports the file in the subdirectory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(sftp.fastGet).to.have.been.calledWith('source/mysubdir/myfile', 'destination/mysubdir/myfile')
        })
      })

      it('does not import an excluded file', function () {
        const filters = '- /mysubdir/myfile'
        return ssh.importFiles({}, 'source', 'destination', filters, stream).then(() => {
          expect(sftp.fastGet).to.have.not.been.called()
        })
      })

      it('preserves the timestamps of the subdirectory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(fs.utimes).to.have.been.calledWith('destination/mysubdir', 12312312, 22322312)
        })
      })
    })

    describe('with a sub-subdirectory', function () {
      beforeEach(function () {
        sftp.readdir.withArgs('source').yieldsAsync(null, [directory('myFirstLevelDir')])
        sftp.readdir.withArgs('source/myFirstLevelDir').yieldsAsync(null, [ directory('mySecondLevelDir') ])
        sftp.readdir.withArgs('source/myFirstLevelDir/mySecondLevelDir').yieldsAsync(null, [ file('myfile') ])
      })

      it('creates the local sub-subdirectory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(fs.mkdir).to.have.been.calledThrice()
          expect(fs.mkdir).to.have.been.calledWith('destination')
          expect(fs.mkdir).to.have.been.calledWith('destination/myFirstLevelDir')
          expect(fs.mkdir).to.have.been.calledWith('destination/myFirstLevelDir/mySecondLevelDir')
        })
      })

      it('imports the file in the sub-subdirectory', function () {
        return ssh.importFiles({}, 'source', 'destination', null, stream).then(() => {
          expect(sftp.fastGet).to.have.been.calledWith(
            'source/myFirstLevelDir/mySecondLevelDir/myfile',
            'destination/myFirstLevelDir/mySecondLevelDir/myfile'
          )
        })
      })

      it('does not import an excluded file', function () {
        const filters = '- /myFirstLevelDir/mySecondLevelDir/myfile'
        return ssh.importFiles({}, 'source', 'destination', filters, stream).then(() => {
          expect(sftp.fastGet).to.have.not.been.called()
        })
      })
    })
  })
})
