/**
 * Integration tests for ssh wrapper.
 */

'use strict'
/* eslint-env mocha */

const fs = require('fs-extra')
const os = require('os')
const path = require('path')
const moment = require('moment')
const concat = require('concat-stream')
const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const ssh = require('../../src/ssh')

describe('ssh', function () {
  const connectionOptions = {
    host: 'cls.wysiwyg.fi',
    username: 'wysiwyg',
    privateKey: fs.readFileSync('/Users/jarno/.ssh/id_rsa')
  }

  describe('#executeAndReturnString', function () {
    it('resolves stdout as a string', function () {
      return expect(ssh.executeAndReturnString(connectionOptions, 'pwd')).to.eventually.equal('/home/wpdemo\n')
    })

    it('rejects if command fails', function () {
      return expect(ssh.executeAndReturnString(connectionOptions, 'command-that-does-not-exist'))
        .to.be.rejectedWith(ssh.SshError, 'command exited with error code 127')
    })

    it('rejects if username is incorrect', function () {
      let connectionOptions2 = Object.assign({}, connectionOptions, { username: 'username-that-does-not-exist' })
      return expect(ssh.executeAndReturnString(connectionOptions2, 'pwd'))
        .to.be.rejectedWith('All configured authentication methods failed')
    })

    it('rejects if host is incorrect', function () {
      let connectionOptions2 = Object.assign({}, connectionOptions, { host: 'host-that-does-not-exist' })
      return expect(ssh.executeAndReturnString(connectionOptions2, 'pwd'))
        .to.be.rejectedWith('getaddrinfo ENOTFOUND host-that-does-not-exist host-that-does-not-exist:22')
    })
  })

  describe('#executeAndReturnStream', function (done) {
    it('resolves as a stream', function () {
      ssh.executeAndReturnStream(connectionOptions, 'pwd')
        .then((stream) => {
          stream.pipe(concat({ encoding: 'string' }, (stdout) => {
            expect(stdout.to.equal('/home/wpdemo\n'))
            done()
          }))
        })
        .catch(done)
    })

    it('rejects if username is incorrect', function () {
      let connectionOptions2 = Object.assign({}, connectionOptions, { username: 'username-that-does-not-exist' })
      return expect(ssh.executeAndReturnStream(connectionOptions2, 'pwd'))
        .to.be.rejectedWith('All configured authentication methods failed')
    })

    it('rejects if host is incorrect', function () {
      let connectionOptions2 = Object.assign({}, connectionOptions, { host: 'host-that-does-not-exist' })
      return expect(ssh.executeAndReturnStream(connectionOptions2, 'pwd'))
        .to.be.rejectedWith('getaddrinfo ENOTFOUND host-that-does-not-exist host-that-does-not-exist:22')
    })
  })

  describe('#importFiles', function () {
    this.timeout(120000)

    const workDir = path.join(os.tmpdir(), 'ssh.importFiles-' + moment().format('YYYYMMDDTHHmmss'))

    before('Import files', function () {
      console.log(`workDir: ${workDir}`)
      return ssh.importFiles(connectionOptions, 'public_html', workDir)
        .catch((err) => {
          console.dir(err)
          throw (err)
        })
    })

    it('imports files in top-level directory', function () {
      return expect(fs.pathExists(path.join(workDir, 'license.txt'))).to.eventually.be.true
    })

    it('imports files in second-level directory', function () {
      return expect(fs.pathExists(path.join(workDir, 'wp-content', 'index.php'))).to.eventually.be.true
    })

    it('imports files in third-level directory', function () {
      return expect(fs.pathExists(path.join(workDir, 'wp-content', 'themes', 'index.php'))).to.eventually.be.true
    })

    after('Clean up the directory created for the test', function () {
      // return fs.remove(workDir)
    })
  })
})
