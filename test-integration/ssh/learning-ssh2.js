/**
 * Tests for learning how to use ssh2 node module.
 */

'use strict'
/* eslint-env mocha */

const SshClient = require('ssh2').Client
const Promise = require('bluebird')
const fs = require('fs')
const chai = require('chai')
const expect = chai.expect

describe('execute pwd over ssh', function () {
  it('should execute pwd on remote server', function (done) {
    const conn = Promise.promisifyAll(new SshClient())

    function exec (cmd) {
      return new Promise((resolve, reject) => {
        let stdout = ''
        let stderr = ''

        conn.execAsync(cmd).then((stream) => {
          stream.on('close', (code, signal) => {
            if (code === 0) {
              resolve({
                stdout: stdout,
                stderr: stderr
              })
            } else {
              reject(new Error(stderr))
            }
          })
          .on('data', function (data) {
            stdout += data
          })
          .stderr.on('data', function (data) {
            stderr += data
          })
        })
      })
    }

    conn.on('ready', () => {
      exec('pwd').then((result) => {
        expect(result.stdout).to.equal('/home/wysiwyg\n')
      })
      .then(() => {
        return exec('wp core version --path=public_html')
      })
      .then((result) => {
        expect(result.stdout).to.equal('4.8.1\n')
      })
      .then(() => {
        conn.end()
      })
      .catch(done)
    })
    .on('end', () => {
      done()
    })
    .on('error', (err) => {
      done(err)
    })
    .connect({
      host: 'cls.wysiwyg.fi',
      username: 'wysiwyg',
      privateKey: fs.readFileSync('/Users/jarno/.ssh/id_rsa')
    })
  })
})
