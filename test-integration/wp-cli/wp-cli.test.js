/**
 * Integration tests for wp-cli wrapper.
 */

'use strict'
/* eslint-env mocha */

const fs = require('fs')
const wp = require('../../src/wp-cli')
const chai = require('chai')
const expect = chai.expect
const Readable = require('stream').Readable
const moment = require('moment')

describe('wp-cli', function () {
  const connectionOptions = {
    host: 'cls.wysiwyg.fi',
    username: 'wpdemo',
    privateKey: fs.readFileSync('/Users/jarno/.ssh/id_rsa')
  }

  describe('#configGet', function () {
    it('gets config from remote server', function () {
      return wp.configGet('public_html', connectionOptions)
        .then((config) => {
          expect(config).to.be.an('array').that.deep.includes({ key: 'table_prefix', 'value': 'wp_', type: 'variable' })
        })
    })
  })

  describe('#dbExport', function () {
    this.timeout(5 * 60 * 1000)
    it('gets database dump from remote server', function (done) {
      wp.dbExport('public_html', connectionOptions)
        .then((stream) => {
          let firstChunk
          let lastChunk
          stream
            .on('data', (chunk) => {
              if (!firstChunk) {
                firstChunk = chunk
              }
              lastChunk = chunk
            })
            .on('end', () => {
              expect(firstChunk.toString()).to.include('-- MySQL dump')
              expect(firstChunk.toString()).to.include('Database: wpdemo_wp\n')

              expect(lastChunk.toString()).to.include('-- Dump completed')
              done()
            })
        })
        .catch(done)
    })
  })

  describe('#dbImport and #dbQuery', function () {
    this.timeout(60 * 1000)
    it('runs sql from stream', function () {
      const testValue = 'wpcli_test_' + moment().format('YYYYMMDDTHHmmss')
      const input = new Readable()
      input.push('DROP TABLE IF EXISTS wysiwygdev_wpcli_test;')
      input.push('CREATE TABLE wysiwygdev_wpcli_test (value VARCHAR(30));')
      input.push(`INSERT INTO wysiwygdev_wpcli_test VALUES ('${testValue}');`)
      input.push(null)

      return wp
        .dbImport('/Users/jarno/work/wpdemo', input)
        .then(() => wp.dbQuery('/Users/jarno/work/wpdemo', 'SELECT value FROM wysiwygdev_wpcli_test'))
        .then(result => {
          return expect(result).include(testValue)
        })
    })
  })
})
