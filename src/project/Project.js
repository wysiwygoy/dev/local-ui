'use strict'

const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const path = require('path')
const yo = require('../yeoman')
const composer = require('../composer')
const dockerCompose = require('../docker-compose')
const ssh = require('../../src/ssh')
const wp = require('../wp-cli')
const _ = require('lodash')
const concat = require('concat-stream')
const Actions = require('../actions')

class Project {
  /**
   * Instantiates a project.
   *
   * @param {string} slug name for the project
   * @param {string} directory directory where the project resides (possibly in Docker container)
   * @param {string} hostDirectory directory where the project resides on Docker host
   *                 (because this might be running in a Docker container,
   *                 we need to tell the 'real' directory to Docker when running composer on Docker).
   */
  constructor (slug, directory, hostDirectory) {
    this.slug = slug
    this.directory = directory
    // noinspection JSUnusedGlobalSymbols
    this.hostDirectory = hostDirectory
  }

  /**
   * Creates a new project from scratch.
   *
   * @param {Object} environment static environment settings for projects
   * @param {string} environment.workDir parent directory for the project in Docker container
   *                 (the project directory will be created under this directory)
   * @param {string} environment.hostWorkDir parent directory for the project on Docker host
   *                 (because this might be running in a Docker container,
   *                 we need to tell the 'real' directory to Docker when running composer on Docker).
   * @param {string} slug machine-friendly name for the project
   * @param {string} adminUser name of the admin user
   * @param {string} adminPassword password for the admin user
   * @param {string} adminEmail email address for the admin user
   * @param {string} deployHost host where the project will be deployed
   * @param {string} deployUser username to use when deploying
   * @param {Writable} output output stream for events
   *
   * @returns Promise with new Project
   */
  static createFromScratch (environment, slug, adminUser, adminPassword, adminEmail, deployHost, deployUser, output) {
    const containerDirectory = path.join(environment.workDir, slug)
    const hostDirectory = path.join(environment.hostWorkDir, slug) // how about on windows?

    return Promise
      .try(() => output.write(Actions.startTask('createFromScratch', { directory: hostDirectory })))
      .then(wpConfig => createProjectScaffolding(
        hostDirectory,
        containerDirectory,
        wpConfig,
        'wysiwyg',
        deployHost,
        deployUser,
        output
      ))
      .then(() => composerInstall(hostDirectory, output))
      .then(() => startServers(hostDirectory, output))
      .then(() => waitForDatabase(hostDirectory, slug, output))
      .then(() => installWordPress(
        hostDirectory,
        `${slug}.local.wysiwyg.fi`,
        slug,
        adminUser,
        adminPassword,
        adminEmail,
        output
      ))
      .then(() => {
        output.write(Actions.finishTask('createFromScratch'))
        return new Project(slug, containerDirectory, hostDirectory)
      })
  }

  /**
   * Creates a new project by importing files from an existing site.
   *
   * @param {Object} environment static environment settings for projects
   * @param {string} environment.workDir parent directory for the project in Docker container
   *                 (the project directory will be created under this directory)
   * @param {string} environment.hostWorkDir parent directory for the project on Docker host
   *                 (because this might be running in a Docker container,
   *                 we need to tell the 'real' directory to Docker when running composer on Docker).
   * @param {Object} sshOptions parameters for SSH connection to remote host
   * @param {string} sshOptions.host remote host where to get files
   * @param {string} sshOptions.username username to use when connecting to the remote host
   * @param {string} sshOptions.password password for the user (if password authentication is used)
   * @param {string} sshOptions.privateKey SSH private key (if key authentication is used)
   * @param {string} remoteDirectory directory on the remote host where WordPress is installed
   * @param {string} slug machine-friendly name for the project
   * @param {Writable} output output stream for events
   *
   * @returns Promise with new Project
   */
  static createFromExistingSite (environment, sshOptions, remoteDirectory, slug, output) {
    const containerDirectory = path.join(environment.workDir, slug)
    const hostDirectory = path.join(environment.hostWorkDir, slug) // how about on windows?

    return Promise
      .try(() => {
        const {host, username} = sshOptions
        output.write(Actions.startTask(
          'createFromExistingSite',
          { host, username, remoteDirectory, localDirectory: hostDirectory }
        ))
      })
      .then(() => getWpConfig(sshOptions, remoteDirectory, output))
      .then(wpConfig => createProjectScaffolding(
        hostDirectory,
        containerDirectory,
        wpConfig,
        'traditional',
        sshOptions.host,
        sshOptions.user,
        output
      ))
      .then(() => composerInstall(hostDirectory, output))
      .then(() => startServers(hostDirectory, output))
      .then(() => importFiles(sshOptions, remoteDirectory, hostDirectory, containerDirectory, output))
      .then(() => waitForDatabase(hostDirectory, slug, output))
      .then(() => importDatabase(sshOptions, remoteDirectory, hostDirectory, output))
      .then(() => getSiteUrl(sshOptions, remoteDirectory, output))
      .then(siteUrl => replaceUrls(hostDirectory, slug, siteUrl, output))
      .then(() => {
        console.log('Done!')
        output.write(Actions.finishTask('createFromExistingSite'))
        return new Project(slug, containerDirectory, hostDirectory)
      })
  }
}

function getWpConfig (sshOptions, directory, output) {
  const { host, username } = sshOptions
  output.write(Actions.startTask('getWpConfig', { host, username, directory }))

  return wp.configGet(directory, sshOptions)
    .then(config => {
      output.write(Actions.finishTask('getWpConfig'))
      return config
    })
    .catch(e => {
      output.write(Actions.taskFailed('getWpConfig', e))
      throw e
    })
}

function createProjectScaffolding (hostDirectory, directory, wpConfig, projectModel, deployHost, deployUser, output) {
  output.write(Actions.startTask('createProjectScaffolding', { directory: hostDirectory }))

  const tablePrefixConfig = wpConfig ? _.find(wpConfig, { key: 'table_prefix' }) : undefined
  const databasePrefix = tablePrefixConfig ? tablePrefixConfig['value'] : undefined

  return fs.mkdirAsync(directory)
    .then(() => yo.generate(
      'generator-wysiwygoy-wordpress',
      directory,
      projectModel,
      { databasePrefix,
        deployHost,
        deployUser
      }
    ))
    .then(() => output.write(Actions.finishTask('createProjectScaffolding')))
    .catch(e => {
      output.write(Actions.taskFailed('createProjectScaffolding', e))
      throw e
    })
}

function composerInstall (hostDirectory, output) {
  output.write(Actions.startTask('composerInstall'))

  return composer
    .install(hostDirectory)
    .then(() => output.write(Actions.finishTask('composerInstall')))
    .catch(e => {
      output.write(Actions.taskFailed('composerInstall', e))
      throw e
    })
}

function startServers (hostDirectory, output) {
  output.write(Actions.startTask('startServers'))

  return dockerCompose.up(hostDirectory)
    .then(() => output.write(Actions.finishTask('startServers')))
    .catch(e => {
      output.write(Actions.taskFailed('startServers', e))
      throw e
    })
}

function waitForDatabase (hostDirectory, slug, output) {
  output.write(Actions.startTask('waitForDatabase'))
  let stdout
  const stdoutConcatStream = concat({ encoding: 'string' }, data => { stdout = data })

  return dockerCompose
    .run(
      hostDirectory,
      'php-cli',
    [ 'wait-for-it.sh',
      `--host=${slug}-db`,
      '--port=3306',
      '--timeout=60'
    ],
      stdoutConcatStream
    )
    .then(() => output.write(Actions.finishTask('waitForDatabase')))
    .then(() => stdout)
    .catch(e => {
      output.write(Actions.taskFailed('waitForDatabase', e))
      throw e
    })
}

function installWordPress (hostProjectDirectory, url, title, adminUser, adminPassword, adminEmail, output) {
  output.write(Actions.startTask('installWordPress'))

  return wp.coreInstall(hostProjectDirectory, url, title, adminUser, adminPassword, adminEmail)
    .then(() => output.write(Actions.finishTask('installWordPress')))
    .catch(e => {
      output.write(Actions.taskFailed('installWordPress', e))
      throw e
    })
}

function importFiles (sshOptions, remoteDirectory, hostProjectDirectory, containerProjectDirectory, output) {
  const containerDirectory = path.join(containerProjectDirectory, 'site', 'public') // TODO: don't hardcode
  const hostDirectory = path.join(hostProjectDirectory, 'site', 'public') // TODO: don't hardcode

  const { host, username } = sshOptions
  output.write(Actions.startTask('importFiles', { host, username, remoteDirectory, localDirectory: hostDirectory }))

  return fs.readFileAsync(path.join(containerProjectDirectory, 'config', 'import-files'), 'utf8')
    .then(filters => ssh.importFiles(sshOptions, remoteDirectory, containerDirectory, filters, output))
    .then(() => output.write(Actions.finishTask('importFiles')))
    .catch(e => {
      output.write(Actions.taskFailed('importFiles', e))
      throw e
    })
}

function importDatabase (sshOptions, remoteDirectory, hostProjectDirectory, output) {
  const { host, username } = sshOptions
  output.write(Actions.startTask('importDatabase', { host, username }))

  return wp.dbExport(remoteDirectory, sshOptions)
    .then(stream => wp.dbImport(hostProjectDirectory, stream))
    .then(() => output.write(Actions.finishTask('importDatabase')))
    .catch(e => {
      output.write(Actions.taskFailed('importDatabase', e))
      throw e
    })
}

function getSiteUrl (sshOptions, remoteDirectory, output) {
  const { host, username } = sshOptions
  output.write(Actions.startTask('getSiteUrl', { host, username, remoteDirectory }))

  return wp.optionGet(remoteDirectory, 'siteurl', sshOptions)
    .then(siteurl => {
      output.write(Actions.finishTask('getSiteUrl'))
      return siteurl
    })
    .catch(e => {
      output.write(Actions.taskFailed('getSiteUrl', e))
      throw e
    })
}

function replaceUrls (hostDirectory, slug, remoteSiteUrl, output) {
  const localSiteUrl = `http://${slug}.local.wysiwyg.fi`
  output.write(Actions.startTask('replaceUrls', { remoteSiteUrl, localSiteUrl }))

  return wp.searchReplace(hostDirectory, remoteSiteUrl, localSiteUrl)
    .then(() => output.write(Actions.finishTask('replaceUrls')))
    .catch(e => {
      output.write(Actions.taskFailed('replaceUrls', e))
      throw e
    })
}

module.exports = Project
