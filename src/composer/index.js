/**
 * Wrapper for running Composer (the PHP one) on Docker.
 */

'use strict'

const Promise = require('bluebird')
const Docker = require('dockerode')

module.exports = {
    /**
     * Runs "composer install" in the given directory.
     *
     * @param hostDirectory directory where to run it
     * @returns Promise
     */
  install: (hostDirectory) => {
    const docker = new Docker()

    // Promisify not used because docker.run returns EventEmitter
    // and we might use it for something some day:
    return new Promise((resolve, reject) => {
      pull()

      function pull () {
        docker.pull(
          'composer:latest',
          (err, stream) => {
            if (err) {
              reject(err)
            } else {
              docker.modem.followProgress(stream, run)
            }
          }
        )
      }

      function run () {
        docker.run(
          'composer:latest', // image
          'install', // command
          process.stdout, // stream for output
          { Binds: [hostDirectory + ':/app'] }, // container create options
          (err, data) => { // callback
            if (err) {
              reject(err)
            }
            resolve(data)
          }
        )
      }
    })
  }
}
