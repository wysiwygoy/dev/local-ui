'use strict'

const Minimatch = require('minimatch').Minimatch

class FileFilterRule {
  constructor (include, pattern) {
    this.include = include
    this.minimatch = new Minimatch(pattern, { matchBase: true })
  }

  static fromString (ruleLine) {
    const comment = /^#.*/
    const empty = /^\s*$/

    // Ignore comments
    if (comment.test(ruleLine)) {
      return null
    }

    // Ignore empty lines
    if (empty.test(ruleLine)) {
      return null
    }

    const matches = ruleLine.match(/^(exclude|-|include|\+)[ _](.*)$/)
    if (matches === null) {
      throw new Error('Invalid filter rule: ' + ruleLine)
    }

    const [, rule, pattern] = matches
    return new FileFilterRule(
      rule === 'include' || rule === '+',
      pattern
    )
  }

  includes (filename) {
    return this.include && this.minimatch.match(filename)
  }

  excludes (filename) {
    return !this.include && this.minimatch.match(filename)
  }
}

module.exports = FileFilterRule
