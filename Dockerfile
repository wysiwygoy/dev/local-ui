#
# User interface for Wysiwyg Oy development environment
#
# Based on https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
#

FROM node:8-alpine
MAINTAINER Jarno Antikainen <jarno.antikainen@wysiwyg.fi>

LABEL Description="This image runs the user interface of Wysiwyg Oy development environment" Vendor="Wysiwyg Oy"

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN apk add --no-cache git && \
    npm install && \
    apk del git

# Bundle app source
COPY ./src /usr/src/app/src
COPY ./test /usr/src/app/test
COPY ./test-integration /usr/src/app/test-integration

ENV PORT 80
EXPOSE 80
ENTRYPOINT ["npm"]
CMD ["start"]
